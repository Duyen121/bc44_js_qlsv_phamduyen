var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};

var isIdDuplicated = function (idCode, studList) {
  var index = studList.findIndex(function (item) {
    return idCode == item.idCode;
  });
  if (index == -1) {
    showMessage("spanMaSV", "");
    return true;
  } else {
    showMessage("spanMaSV", "Mã sinh viên đã tồn tại");
    return false;
  }
};

var isRequiredField = function (idErr, value) {
  if (value.length == 0) {
    showMessage(idErr, "Thông tin không được bỏ trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};

var isEmailEmpty = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("spanEmailSV", "");
    return true;
  } else {
    showMessage("spanEmailSV", "Email không hợp lệ");
    return false;
  }
};
