function getStudInfo() {
  var idCode = document.getElementById("txtMaSV").value;
  var name = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var mathScore = document.getElementById("txtDiemToan").value * 1;
  var physScore = document.getElementById("txtDiemLy").value * 1;
  var chemScore = document.getElementById("txtDiemHoa").value * 1;

  var studInfo = new student(
    idCode,
    name,
    email,
    password,
    mathScore,
    physScore,
    chemScore
  );
  return studInfo;
}

function displayStudInfo(studInfo) {
  document.getElementById("txtMaSV").value = studInfo.idCode;
  document.getElementById("txtTenSV").value = studInfo.name;
  document.getElementById("txtEmail").value = studInfo.email;
  document.getElementById("txtPass").value = studInfo.password;
  document.getElementById("txtDiemToan").value = studInfo.mathScore;
  document.getElementById("txtDiemLy").value = studInfo.physScore;
  document.getElementById("txtDiemHoa").value = studInfo.chemScore;
}

function renderStudList(studArr) {
  var HTMLContent = "";
  for (var i = 0; i < studArr.length; i++) {
    var studInfo = studArr[i];
    var trContent = `<tr>
    <td>${studInfo.idCode}</td>
    <td>${studInfo.name}</td>
    <td>${studInfo.email}</td>
    <td>${studInfo.calMeanScore()}</td>
    <td>
      <button onclick="deleteStudent(${
        studInfo.idCode
      })" class="btn btn-danger">Xoá</button>
      <button onclick="editStudent(${
        studInfo.idCode
      })" class="btn btn-secondary">Sửa</button>
    </td>
    </tr>`;

    HTMLContent = HTMLContent + trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = HTMLContent;
}
