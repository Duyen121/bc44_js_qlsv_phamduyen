function student(
  _idCode,
  _name,
  _email,
  _password,
  _mathScore,
  _physScore,
  _chemScore
) {
  this.idCode = _idCode;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.mathScore = _mathScore;
  this.physScore = _physScore;
  this.chemScore = _chemScore;
  this.calMeanScore = function () {
    return ((this.mathScore + this.physScore + this.chemScore) / 3).toFixed(2);
  };
}
