var studList = [];
var dataJSON = localStorage.getItem("DATA_LOCAL");
var validation = function () {};

if (dataJSON != null) {
  var dataArr = JSON.parse(dataJSON);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var studInfo = new student(
      item.idCode,
      item.name,
      item.email,
      item.password,
      item.mathScore,
      item.physScore,
      item.chemScore
    );
    studList.push(studInfo);
  }
  renderStudList(studList);
}

function saveDataInLocalStor(data) {
  var dataJSON = JSON.stringify(data);
  localStorage.setItem("DATA_LOCAL", dataJSON);
}

function addStudent() {
  var student = getStudInfo();
  var isValid =
    isRequiredField("spanMaSV", student.idCode) &&
    isIdDuplicated(student.idCode, studList);
  isValid =
    isValid &
    isRequiredField("spanTenSV", student.name) &
    isRequiredField("spanMatKhau", student.password);
  isValid =
    isValid & isRequiredField("spanEmailSV", student.email) &&
    isEmailEmpty(student.email);

  if (isValid) {
    studList.push(student);
    saveDataInLocalStor(studList);
    renderStudList(studList);
  } else {
    alert("Bạn chưa điền đủ 4 thông tin đầu tiên hoặc Mã sinh viên đã tồn tại");
  }
}

function deleteStudent(id) {
  var location = -1;
  for (let i = 0; i < studList.length; i++) {
    var student = studList[i];
    if (student.idCode == id) {
      location = i;
    }
  }
  studList.splice(location, 1);
  saveDataInLocalStor(studList);
  renderStudList(studList);
}

function editStudent(id) {
  $(".text-danger").hide();
  var location = studList.findIndex(function (item) {
    return item.idCode == id;
  });
  if (location != -1) {
    document.getElementById("txtMaSV").disabled = true;
    displayStudInfo(studList[location]);
  }
}

function updateStudent() {
  // document.getElementById("txtMaSV").disabled = false;
  var student = getStudInfo();
  var location = studList.findIndex(function (item) {
    return item.idCode == student.idCode;
  });
  if (location != -1) {
    studList[location] = student;
    renderStudList(studList);
    saveDataInLocalStor(studList);
  }
}

function resetForm() {
  var idCode = document.getElementById("txtMaSV");
  idCode.disabled = false;
  document.getElementById("formQLSV").reset();
  idCode.value = "";
}

function searchStudent() {
  var searchingStudArr = [];
  var searchingStudName = document.getElementById("txtSearch").value;
  for (var i = 0; i < studList.length; i++) {
    if (searchingStudName === studList[i].name) {
      displayStudInfo(studList[i]);
      document.getElementById("txtMaSV").disabled = true;

      searchingStudArr.push(studList[i]);
      renderStudList(searchingStudArr);
    }
  }
  if (searchingStudArr.length == 0) {
    alert("Tên sinh viên không tồn tại");
  }
}
